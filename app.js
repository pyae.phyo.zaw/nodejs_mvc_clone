const path = require('path'); // core node module
const express = require('express');
const app = express();
const bodyParser = require('body-parser'); // responsible for parsing the incoming request bodies in a middleware before you handle it.
let publicPath = path.join(__dirname, "./public"); // set public path
const port = process.env.PORT || 3000;

app.set('view engine', 'ejs');

app.use(express.static(publicPath));
app.use(bodyParser.json({ limit: '10mb' })); // Controls the maximum request body size.
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true })); // only parses urlencoded bodies // extended : true means any type 
app.use(require('./controllers/home.js'));
app.use(require('./controllers/index.js'));

app.get('/', (req, res) => {
    res.send("Welcome to Main");
});

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
})