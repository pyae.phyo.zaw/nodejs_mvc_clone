const express = require('express');
const router = express.Router();

console.log("Inside Index.js");

router.get("/", (req, res) => {
    res.render("index");
});

router.get("/adduser", (req, res) => {
    res.render("create");
});

router.get("/deleteuser", (req, res) => {
    res.render("delete");
});

router.get("/updateuser", (req, res) => {
    res.render("update");
});

module.exports = router;