const express = require('express');
const router = express.Router();

console.log("Inside Home");

let userModel = require("../models/user_model");

router.post('/users/create', (req, res) => {
    console.log("Create User is loaded");

    let udata = {
        uid: req.body.uid,
        uname: req.body.uname,
        ucoins: req.body.ucoins,
        ugems: req.body.ugems
    };
    userModel.createUser(udata, (err, data) => {
        res.send(data);
    });
});

router.get('/users/show', (req, res) => {
    console.log("Show Users is loaded");
    userModel.showAllUsers((err, data) => {
        res.render("show", { data: data });
    });
});

router.get('/users/delete', (req, res) => {
    console.log("Delete User is loaded");
    let uid = req.query.id;
    console.log(uid);
    userModel.deleteUser(uid, (err, data) => {
        res.send(data);
    })
});

router.post('/users/update', (req, res) => {
    console.log("Update User is loaded");
    let udata = {
        uid: req.body.uid,
        uname: req.body.uname,
        ucoins: req.body.ucoins,
        ugems: req.body.ugems
    };
    userModel.updateUser(udata.uid, udata, (err, data) => {
        // res.redirect("/users/show");
        res.send(data);
    });
});

module.exports = router;