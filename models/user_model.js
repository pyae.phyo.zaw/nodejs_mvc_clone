const getConnection = require('../helpers/database');

// SELECT
exports.showAllUsers = (cb) => {
    let qString = "SELECT * FROM user_infos ORDER BY user_id";

    getConnection((err, connection) => {
        connection.query(qString, (err, result) => {
            connection.release();
            if (!err) {
                cb(null, result);
            } else {
                cb(err, null);
            }
        });
    });
};

// INSERT
exports.createUser = (userData, cb) => {
    let qString = "INSERT INTO user_infos(user_id, user_name, user_coins, user_gems) VALUES("
        + userData.uid + "," + "'" + userData.uname + "'" + "," + userData.ucoins + "," + userData.ugems + ")";

    getConnection((err, connection) => {
        connection.query(qString, (err, result) => {
            connection.release();
            if (!err) cb(null, "Create User Successfully");
            else cb("Fail to Create User", null);
        });
    });
};

// DELETE
exports.deleteUser = (uid, cb) => {
    let qString = "DELETE FROM user_infos WHERE user_id=" + uid;
    console.log(qString);
    getConnection((err, connection) => {
        connection.query(qString, (err, result) => {
            connection.release();
            console.log(result);
            if (!err) cb(null, "Delete User Successfully");
            else cb("Fail to Delete User", null);
        });
    });
};

exports.updateUser = (uid, udata, cb) => {
    let qString = "UPDATE user_infos SET user_name=" + "'" + udata.uname + "'" + ", user_coins=" + udata.ucoins + ", user_gems=" + udata.ugems +
        " WHERE user_id =" + uid;
    getConnection((err, connection) => {
        connection.query(qString, (err, result) => {
            connection.release();
            if (!err) cb(null, "Update User Successfully");
            else cb("Fail to Update User", null);
        })
    })
}

