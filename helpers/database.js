const mysql = require('mysql');

const pool = mysql.createPool({
    connectionLimit: 10,
    host: "localhost",
    user: "sqluser",
    password: "12345",
    database: "my_db",
});

let getConnection = (callback) => {
    pool.getConnection((err, connection) => {
        if (err) throw err;
        console.log("Database is Connected");
        callback(err, connection);
        // return;
    });
}

module.exports = getConnection;